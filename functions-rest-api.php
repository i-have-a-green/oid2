<?php
/*
WP_REST_Server::READABLE = ‘GET’
WP_REST_Server::EDITABLE = ‘POST’
WP_REST_Server::DELETABLE = ‘DELETE’
WP_REST_Server::CREATE = ‘PUT’
*/
add_action('rest_api_init', function() {
	register_rest_route( 'ihag', 'action-rest',
		array(
		'methods' 				=> 'POST', 
		'callback'        		=> 'ihagCallback'
		)
	);
});
function ihagCallback(WP_REST_Request $request){
	if ( check_nonce() ) {
		$params = $request->get_params();
		return new WP_REST_Response( 'Hello World', 200 );
	}
	return new WP_REST_Response( 'BAD NONCE', 401 );
}
