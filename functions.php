<?php
/**
 * DESCRIPTION ihag functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package ihag
 */

if ( ! defined( 'IHAG_VERSION' ) ) {
	define( 'IHAG_VERSION', '1.0.0' );
}

/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function ihag_setup() {
	/*
		* Make theme available for translation.
		* Translations can be filed in the /languages/ directory.
		* If you're building a theme based on ihag, use a find and replace
		* to change 'ihag' to the name of your theme in all the template files.
		*/
	load_theme_textdomain( 'ihag', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
		* Let WordPress manage the document title.
		* By adding theme support, we declare that this theme does not use a
		* hard-coded <title> tag in the document head, and expect WordPress to
		* provide it for us.
		*/
	add_theme_support( 'title-tag' );

	/*
		* Enable support for Post Thumbnails on posts and pages.
		*
		* @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		*/
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 1568, 9999 );
	add_image_size( '150-150', 100, 100 );
	add_image_size( '350-350', 350, 350 );
	add_image_size( '450-450', 450, 450 );
	add_image_size( '650-650', 650, 650 );
	add_image_size( '850-850', 850, 850 );

	/**
	 * Ihag_custom_sizes
	 *
	 * @param  mixed $sizes
	 * @return array
	 */
	function ihag_custom_sizes( $sizes ) {
		return array_merge(
			$sizes,
			array(
				'150-150' => __( '150x150 - Petite sans rognage', 'ihag' ),
				'350-350' => __( '350x350 - Contact sans rognage', 'ihag' ),
				'450-450' => __( '450x450 - Moyen sans rognage', 'ihag' ),
				'650-650' => __( '650x650 - Grande sans rognage', 'ihag' ),
				'650-650' => __( '850x850 - Grande sans rognage', 'ihag' ),
			)
		);
	}
	add_filter( 'image_size_names_choose', 'ihag_custom_sizes' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus(
		array(
			'menu-1' => esc_html__( 'Primary', 'ihag' ),
		)
	);

	/*
		* Switch default core markup for search form, comment form, and comments
		* to output valid HTML5.
		*/
	add_theme_support(
		'html5',
		array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
			'style',
			'script',
		)
	);

	
	
	// Add theme support for selective refresh for widgets.
	add_theme_support( 'customize-selective-refresh-widgets' );

	/**
	 * Add support for core custom logo.
	 *
	 * @link https://codex.wordpress.org/Theme_Logo
	 */

	$logo_width  = 300;
	$logo_height = 100;
	add_theme_support(
		'custom-logo',
		array(
			'height'      => $logo_height,
			'width'       => $logo_width,
			'flex-width'  => true,
			'flex-height' => true,
		)
	);

	// Add theme support for selective refresh for widgets.
	add_theme_support( 'customize-selective-refresh-widgets' );

	// Add support for Block Styles.
	add_theme_support( 'wp-block-styles' );

	// Add support for full and wide align images.
	add_theme_support( 'align-wide' );

	// Add support for editor styles.
	add_theme_support( 'editor-styles' );

	// Color palette.
	$black     = '#000000';
	$dark_gray = '#28303D';
	$gray      = '#39414D';
	$green     = '#D1E4DD';
	$blue      = '#D1DFE4';
	$purple    = '#D1D1E4';
	$red       = '#E4D1D1';
	$orange    = '#E4DAD1';
	$yellow    = '#EEEADD';
	$white     = '#FFFFFF';

	add_theme_support(
		'editor-color-palette',
		array(
			array(
				'name'  => esc_html__( 'Black', 'ihag' ),
				'slug'  => 'black',
				'color' => $black,
			),
			array(
				'name'  => esc_html__( 'Dark gray', 'ihag' ),
				'slug'  => 'dark-gray',
				'color' => $dark_gray,
			),
			array(
				'name'  => esc_html__( 'Gray', 'ihag' ),
				'slug'  => 'gray',
				'color' => $gray,
			),
			array(
				'name'  => esc_html__( 'Green', 'ihag' ),
				'slug'  => 'green',
				'color' => $green,
			),
			array(
				'name'  => esc_html__( 'Blue', 'ihag' ),
				'slug'  => 'blue',
				'color' => $blue,
			),
			array(
				'name'  => esc_html__( 'Purple', 'ihag' ),
				'slug'  => 'purple',
				'color' => $purple,
			),
			array(
				'name'  => esc_html__( 'Red', 'ihag' ),
				'slug'  => 'red',
				'color' => $red,
			),
			array(
				'name'  => esc_html__( 'Orange', 'ihag' ),
				'slug'  => 'orange',
				'color' => $orange,
			),
			array(
				'name'  => esc_html__( 'Yellow', 'ihag' ),
				'slug'  => 'yellow',
				'color' => $yellow,
			),
			array(
				'name'  => esc_html__( 'White', 'ihag' ), 
				'slug'  => 'white',
				'color' => $white,
			),
		)
	);


	// Add support for responsive embedded content.
	add_theme_support( 'responsive-embeds' );

	// Add support for custom line height controls.
	add_theme_support( 'custom-line-height' );

	// Add support for experimental link color control.
	add_theme_support( 'experimental-link-color' );

	// Add support for experimental cover block spacing.
	add_theme_support( 'custom-spacing' );

	// Add support for custom units.
	// This was removed in WordPress 5.6 but is still required to properly support WP 5.5.
	add_theme_support( 'custom-units' );

}
add_action( 'after_setup_theme', 'ihag_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function ihag_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'ihag_content_width', 640 );
}
add_action( 'after_setup_theme', 'ihag_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function ihag_widgets_init() {
	register_sidebar(
		array(
			'name'          => esc_html__( 'Sidebar', 'ihag' ),
			'id'            => 'sidebar-1',
			'description'   => esc_html__( 'Add widgets here.', 'ihag' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
}
add_action( 'widgets_init', 'ihag_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function ihag_scripts() {
	wp_enqueue_style( 'ihag-style', get_stylesheet_uri(), array(), IHAG_VERSION );
	wp_style_add_data( 'ihag-style', 'rtl', 'replace' );

	wp_enqueue_script( 'ihag-navigation', get_template_directory_uri() . '/js/navigation.js', array(), IHAG_VERSION, true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	wp_enqueue_script( 'ihag-script', get_template_directory_uri() . '/js/script.js', array(), IHAG_VERSION, true );
	wp_localize_script('ihag-script', 'resturl', site_url() . '/wp-json/ihag/');
	wp_localize_script( 'ihag-script', 'wpApiSettings', array(
		'root' => esc_url_raw( rest_url() ),
		'nonce' => wp_create_nonce( 'wp_rest' )
	) );

}
add_action( 'wp_enqueue_scripts', 'ihag_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';



/**
 * Propose les plugins à télécharger.
 */
require get_template_directory() . '/inc/plugin-require.php';

/**
 * Load WooCommerce compatibility file.
 */
if ( class_exists( 'WooCommerce' ) ) {
	require get_template_directory() . '/inc/woocommerce.php';
}

/**
 * Ihag_title_cat
 *
 * @param  mixed $title
 * @return string
 */
function ihag_title_cat( $title ) {
	if ( is_category() ) {
		$title = single_cat_title( '', false );
	} elseif ( is_tag() ) {
		$title = single_tag_title( '', false );
	} elseif ( is_author() ) {
		$title = get_the_author();
	} elseif ( is_tax() ) { 
		$title = single_term_title( '', false );
	} elseif ( is_post_type_archive() ) {
		$title = post_type_archive_title( '', false );
	}
	return $title;
}
add_filter( 'get_the_archive_title', 'ihag_title_cat' );


/**
 * My_acf_json_save_point
 *
 * @param  mixed $path
 * @return string
 */
function ihag_acf_json_save_point( $path ) {
	$path = get_stylesheet_directory() . '/jsonACF';
	if ( ! file_exists( $path ) ) {
		mkdir( $path, 0777 );}
	return $path;
}
add_filter( 'acf/settings/save_json', 'ihag_acf_json_save_point' );


/**
 * My_acf_json_load_point
 *
 * @param  mixed $paths
 * @return string
 */
function ihag_acf_json_load_point( $paths ) {
	unset( $paths[0] );
	$paths[] = get_stylesheet_directory() . '/jsonACF';
	return $paths;
}
add_filter( 'acf/settings/load_json', 'ihag_acf_json_load_point' );


if ( ! defined( 'WP_POST_REVISIONS' ) ) {
	define( 'WP_POST_REVISIONS', 3 );
}


/**
 * Ihag_revision_number
 *
 * @param  mixed $num
 * @param  mixed $post
 * @return int
 */
function ihag_revision_number( $num, $post ) {
	return 3;
}
add_filter( 'wp_revisions_to_keep', 'ihag_revision_number', 4, 2 );

// un intervalle entre deux sauvegardes de 360 secondes.
if ( ! defined( 'AUTOSAVE_INTERVAL' ) ) {
	define( 'AUTOSAVE_INTERVAL', 360 );
}


/**
 * Ihag_clean_head
 * 
 * @link https://crunchify.com/how-to-clean-up-wordpress-header-section-without-any-plugin/
 * 
 * @return void
 */
function ihag_clean_head() {
	return '';
}
add_filter( 'the_generator', 'ihag_clean_head' );

remove_action( 'wp_head', 'rest_output_link_wp_head', 10 );
remove_action( 'wp_head', 'wp_oembed_add_discovery_links', 10 );
remove_action( 'template_redirect', 'rest_output_link_header', 11, 0 );
remove_action( 'wp_head', 'rsd_link' );
remove_action( 'wp_head', 'wlwmanifest_link' );
remove_action( 'wp_head', 'wp_shortlink_wp_head' );
remove_action( 'wp_head', 'feed_links_extra', 3 ); // Display the links to the extra feeds such as category feeds.
remove_action( 'wp_head', 'feed_links', 2 ); // Display the links to the general feeds: Post and Comment Feed.


/**
 * Ihag_disable_wp_emoji
 *
 * @return void
 */
function ihag_disable_wp_emoji() {
	remove_action( 'admin_print_styles', 'print_emoji_styles' );
	remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
	remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
	remove_action( 'wp_print_styles', 'print_emoji_styles' );
	remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
	remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
	remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
}
add_action( 'init', 'ihag_disable_wp_emoji' );

function check_nonce(){
	global $wp_rest_auth_cookie;
	/*
	 * Is cookie authentication being used? (If we get an auth
	 * error, but we're still logged in, another authentication
	 * must have been used.)
	 */
	if ( true !== $wp_rest_auth_cookie && is_user_logged_in() ) {
		return false;
	}
	// Is there a nonce?
	$nonce = null;
	if ( isset( $_REQUEST['_wp_rest_nonce'] ) ) {
		$nonce = $_REQUEST['_wp_rest_nonce'];
	} elseif ( isset( $_SERVER['HTTP_X_WP_NONCE'] ) ) {
		$nonce = $_SERVER['HTTP_X_WP_NONCE'];
	}
	if ( null === $nonce ) {
		// No nonce at all, so act as if it's an unauthenticated request.
		wp_set_current_user( 0 );
		return false;
	}
	// Check the nonce.
	return wp_verify_nonce( $nonce, 'wp_rest' );
}
