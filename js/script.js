if(document.forms.namedItem("ihag-form")){
    let form = document.forms.namedItem("ihag-form");
    form.addEventListener('submit', function(e) {
        e.preventDefault();
        e.stopPropagation();
        fetch(resturl + 'action-rest', {
            method: 'POST',
            body: new FormData(form),
            headers: {'X-WP-Nonce': wpApiSettings.nonce},
            cache: 'no-cache',
        })
        .then(
            function(response) {
                if (response.status !== 200) { console.log(response.status); return;}
                response.json().then(function(data) {
                    console.log(data);
                });
            }
        )
        .catch(function(err) {
            console.log('Fetch Error :-S', err);
        });
    });
}

