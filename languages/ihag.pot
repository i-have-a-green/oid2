# Copyright (C) 2022 Automattic
# This file is distributed under the GNU General Public License v2 or later.
msgid ""
msgstr ""
"Project-Id-Version: ihag 1.0.0\n"
"Report-Msgid-Bugs-To: https://wordpress.org/support/theme/starter-theme-underscore\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"POT-Creation-Date: 2022-04-05T09:54:51+00:00\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"X-Generator: WP-CLI 2.6.0\n"
"X-Domain: ihag\n"

#. Theme Name of the theme
msgid "ihag"
msgstr ""

#. Theme URI of the theme
msgid "https://underscores.me/"
msgstr ""

#. Description of the theme
msgid "Hi. I'm a starter theme called <code>_s</code>, or <em>underscores</em>, if you like. I'm a theme meant for hacking so don't use me as a <em>Parent Theme</em>. Instead try turning me into the next, most awesome, WordPress theme out there. That's what I'm here for."
msgstr ""

#. Author of the theme
msgid "Automattic"
msgstr ""

#. Author URI of the theme
msgid "https://automattic.com/"
msgstr ""

#: 404.php:22
msgid "Oops! That page can&rsquo;t be found."
msgstr ""

#: 404.php:26
msgid "It looks like nothing was found at this location. Maybe try one of the links below or a search?"
msgstr ""

#: 404.php:35
msgid "Most Used Categories"
msgstr ""

#. translators: %1$s: smiley
#: 404.php:53
msgid "Try looking in the monthly archives. %1$s"
msgstr ""

#. translators: 1: title.
#: comments.php:35
msgid "One thought on &ldquo;%1$s&rdquo;"
msgstr ""

#. translators: 1: comment count number, 2: title.
#: comments.php:41
msgctxt "comments title"
msgid "%1$s thought on &ldquo;%2$s&rdquo;"
msgid_plural "%1$s thoughts on &ldquo;%2$s&rdquo;"
msgstr[0] ""
msgstr[1] ""

#: comments.php:68
msgid "Comments are closed."
msgstr ""

#: functions.php:71
msgid "Primary"
msgstr ""

#: functions.php:154
msgid "Black"
msgstr ""

#: functions.php:159
msgid "Dark gray"
msgstr ""

#: functions.php:164
msgid "Gray"
msgstr ""

#: functions.php:169
msgid "Green"
msgstr ""

#: functions.php:174
msgid "Blue"
msgstr ""

#: functions.php:179
msgid "Purple"
msgstr ""

#: functions.php:184
msgid "Red"
msgstr ""

#: functions.php:189
msgid "Orange"
msgstr ""

#: functions.php:194
msgid "Yellow"
msgstr ""

#: functions.php:199
msgid "White"
msgstr ""

#: functions.php:245
msgid "Sidebar"
msgstr ""

#: functions.php:247
msgid "Add widgets here."
msgstr ""

#: header.php:26
msgid "Skip to content"
msgstr ""

#: header.php:49
msgid "Primary Menu"
msgstr ""

#. translators: %s: search query.
#: search.php:21
msgid "Search Results for: %s"
msgstr ""

#: single.php:21
msgid "Previous:"
msgstr ""

#: single.php:22
msgid "Next:"
msgstr ""

#: template-parts/content-none.php:14
msgid "Nothing Found"
msgstr ""

#. translators: 1: link to WP admin new post page.
#: template-parts/content-none.php:24
msgid "Ready to publish your first post? <a href=\"%1$s\">Get started here</a>."
msgstr ""

#: template-parts/content-none.php:37
msgid "Sorry, but nothing matched your search terms. Please try again with some different keywords."
msgstr ""

#: template-parts/content-none.php:44
msgid "It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help."
msgstr ""

#: template-parts/content-page.php:25
#: template-parts/content.php:53
msgid "Pages:"
msgstr ""

#. translators: %s: Name of current post. Only visible to screen readers
#: template-parts/content-page.php:39
msgid "Edit <span class=\"screen-reader-text\">%s</span>"
msgstr ""

#. translators: %s: Name of current post. Only visible to screen readers
#: template-parts/content.php:40
msgid "Continue reading<span class=\"screen-reader-text\"> \"%s\"</span>"
msgstr ""
